@extends('layouts.main')

@section('content')

    <h3>The 3 Most-Popular CryptoCurrencies</h3>
    @foreach($pop_currencies as $currency)
        @php
            $currency_data = \App\Services\CurrencyPresenter::present($currency);
        @endphp
        <div class="currency" id="currency_{{$currency_data['id']}}">
            <img src="{{$currency_data['img']}}" alt="{{$currency_data['name']}}">
            {{$currency_data['name']}} : ${{$currency_data['price']}}
        </div>
    @endforeach

@endsection