<?php

namespace App\Providers;

use App\Services\CurrencyRepositoryInterface;
use App\Services\CurrencyGenerator;
use Illuminate\Support\ServiceProvider;

class CurrencyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CurrencyRepositoryInterface::class, function(){
            $array = CurrencyGenerator::generate();
            return new CurrencyGenerator($array);
        });
    }
}
