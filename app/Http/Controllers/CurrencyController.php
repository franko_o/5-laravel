<?php

namespace App\Http\Controllers;
use App\Services\CurrencyPresenter;
use App\Services\CurrencyRepositoryInterface;
use App\Services\GetMostChangedCurrencyCommandHandler;
use App\Services\GetPopularCurrenciesCommandHandler;

class CurrencyController extends Controller
{
    public function get(CurrencyRepositoryInterface $currencyRepository)
    {
        $currencies = $currencyRepository->findAll();
        foreach ($currencies as &$currency) {
            $currency = CurrencyPresenter::present($currency);
        }
        return response()->json($currencies);
    }

    public function topUnstable(CurrencyRepositoryInterface $currencyRepository)
    {
        $changed = new GetMostChangedCurrencyCommandHandler($currencyRepository);
        $result = $changed->handle();
        return response()->json(CurrencyPresenter::present($result));
    }

    public function popular(CurrencyRepositoryInterface $currencyRepository)
    {
        $pop = new GetPopularCurrenciesCommandHandler($currencyRepository);
        $pop_currencies = $pop->handle(3);
        return view('popular_currencies', compact('pop_currencies'));
    }
}
