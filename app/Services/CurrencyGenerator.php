<?php

namespace App\Services;

use App\Services\CurrencyRepositoryInterface;

class CurrencyGenerator implements CurrencyRepositoryInterface
{
    /**
     * @var Currency[]
     */
    private $currencies;

    /**
     * Generate currencies
     * @return array
     */
    public static function generate(): array
    {
        $currencies = new GetCurrenciesCommandHandler();
        return $currencies->handle();
    }

    /**
     * CurrencyGenerator constructor.
     * @param array $currencies
     */
    public function __construct(array $currencies)
    {
        $this->currencies = $currencies;
    }

    public function findAll(): array
    {
        return $this->currencies;
    }
}