<?php

namespace App\Services;

class GetMostChangedCurrencyCommandHandler
{
    /**
     * @var Currency[]|array
     */
    private $currencies;

    public function __construct(CurrencyRepositoryInterface $currencyRepository)
    {
        $this->currencies = $currencyRepository->findAll();
    }

    public function handle(): Currency
    {
        $sorted_collection = $this->currencies;

        usort($sorted_collection, function(Currency $a, Currency $b) {
            return $b->getDailyChangePercent() <=> $a->getDailyChangePercent() ;
        });

        return $sorted_collection[0];
    }
}