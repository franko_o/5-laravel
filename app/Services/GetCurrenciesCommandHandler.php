<?php

namespace App\Services;

use GuzzleHttp\Client;

/**
 * Class GetCurrenciesCommandHandler
 * @package App\Services
 */
class GetCurrenciesCommandHandler
{
    const API_URL = 'https://api.coinmarketcap.com/v2/ticker/';
    const IMAGE = 'http://cynic.me/wp-content/uploads/2013/12/CryptoBackup256.png';

    /**
     * Get all currencies from Coinmarketcap
     *
     * @return array
     * @throws \Exception
     */
    public function handle(): array
    {
        $client = new Client();
        $res = $client->request('GET', self::API_URL);

        if ($res->getStatusCode() != 200) {
            throw new \Exception('Bad request');
        }

        $result = $res->getBody();

        if (empty($result)) {
            throw new \Exception('Something is wrong');
        }

        $currencies = json_decode($result, true);
        $currencies_data = $currencies['data'];

        $collection = [];
        foreach ($currencies_data as $item) {
            $currency = new Currency(
                $item['id'], $item['name'], $item['quotes']['USD']['price'],
                self::IMAGE, $item['quotes']['USD']['percent_change_24h']
            );

            $collection[] = $currency;
        }
        return $collection;
    }
}