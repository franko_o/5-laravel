<?php

namespace App\Services;

/**
 * Class Currency
 * @package App\Services
 */
class Currency
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $price;

    /**
     * @var string
     */
    private $image_url;

    /**
     * @var int
     */
    private $daily_change_percent;

    /**
     * Currency constructor.
     * @param int $id
     * @param string $name
     * @param float $price
     * @param string $image_url
     * @param float $daily_change_percent
     */
    public function __construct(int $id, string $name, float $price,
                                string $image_url, float $daily_change_percent)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->image_url = $image_url;
        $this->daily_change_percent = $daily_change_percent;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->image_url;
    }

    /**
     * @return float
     */
    public function getDailyChangePercent()
    {
        return $this->daily_change_percent;
    }
}