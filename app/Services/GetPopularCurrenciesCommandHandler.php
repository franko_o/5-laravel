<?php

namespace App\Services;

class GetPopularCurrenciesCommandHandler
{
    const POPULAR_COUNT = 3;

    /**
     * @var Currency[]|array
     */
    private $currencies;

    public function __construct(CurrencyRepositoryInterface $currencyRepository)
    {
        $this->currencies = $currencyRepository->findAll();
    }

    public function handle(int $count = self::POPULAR_COUNT): array
    {
        $sorted_collection = $this->currencies;

        usort($sorted_collection, function(Currency $a, Currency $b) {
            return $b->getPrice() <=> $a->getPrice() ;
        });

        return array_slice($sorted_collection, 0, $count);
    }
}